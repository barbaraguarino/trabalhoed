#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char byte;

typedef struct arvore{
    int frequencia;
    byte c;
    struct arvore *esq;
    struct arvore *dir;
} arvore;

typedef struct filaprioridade{
    arvore *n;
    struct filaprioridade *prox;
} filaprioridade;

typedef struct lista{
    filaprioridade *cabeca;
    int elem;
} lista;


filaprioridade *novofilaprioridade(arvore *nArv){
    filaprioridade *novo;
    novo = malloc(sizeof(*novo));
    if (!novo) return NULL;
    novo->n = nArv;
    novo->prox = NULL;
    return novo;
}

arvore *novoarvore(byte c, int frequencia, arvore *esq, arvore *dir){
    arvore *novo;
    novo = malloc(sizeof(*novo)) 
    if (!novo) return NULL;
    novo->c = c;
    novo->frequencia = frequencia;
    novo->esq = esq;
    novo->dir = dir;
    return novo;
}


void insereLista(filaprioridade *n, lista *l){
    if (!l->cabeca){
        l->cabeca = n;
    }else if((n->n->frequencia) < (l->cabeca->n->frequencia)){
        n->prox = l->cabeca;
        l->v = n;
    }else{

        filaprioridade *aux = l->cabeca->prox;
        filaprioridade *aux2 = l->cabeca;
        //insere ordenado
        while (aux && aux->n->frequencia <= n->n->frequencia){
            aux2 = aux;
            aux = aux2->prox;
        }
        aux2->prox = n;
        n->prox = aux;
    }
    l->elem++;
}

arvore *popmenorfrequenciaLista(lista *l){
    filaprioridade *aux = l->cabeca;
    arvore *aux2 = aux->n;
    l->cabeca = aux->prox;
    free(aux);
    aux = NULL;
    l->elem--;
    return aux2;
}


void frequenciabytes(FILE *entrada, unsigned int *listafreqbytes){
    byte c;
    while ((fread(&c,1,1,entrada)) >= 1){
        listafreqbytes[(byte)c]++;
    }
    rewind(entrada); // "rebobina o arquivo"
}

arvore *arvoreHuffman(unsigned int *listafreqbytes){
    // Lista com no cabeça apontando pra NULL e com campo 'elem' = 0;
    lista l = {NULL, 0};
    for (int i = 0; i < 256; i++){ //i menor que o tamanho max da lista
        if (listafreqbytes[i]){
            insereLista(novofilaprioridade(novoarvore(i, listafreqbytes[i], NULL, NULL)), &l);
        }
    }

    while (l.elem > 1){
        arvore *noesq = popmenorfrequenciaLista(&l);
        arvore *nodir = popmenorfrequenciaLista(&l);
        arvore *soma = novoarvore('#',noesq->frequencia + nodir->frequencia, noesq, nodir);
        insereLista(novofilaprioridade(soma), &l);
    }

    return popmenorfrequenciaLista(&l);
}

void FreearvoreHuffman(arvore *huff){
    if (!huff) return;
    else{
        arvore *esq = huff->esq;
        arvore *dir = huff->dir;
        free(huff);
        FreeHuffmanTree(esq);
        FreeHuffmanTree(dir);
    }
}


//unsigned int listafreqbytes é a lista de frequencia