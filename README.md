**Trabalho ED - 24/11/2019 21:48**

Trabalho da materia de Estrutura de Dados.

Grupo 06: Barbara, Fabio e Camille

--

## Comando GCC

gcc -o x main.c tarvb.c tarvb.h
./x

---

## Arvore B

Árvore B é montada logo apos o usuário inserir um número de T maior ou igual a 2, para montá-la foi utilizado um arquivo em binário preparado anteriormente com todas as informações presentes no arquivo texto 'dados.txt'.

A árvore apresenta a possibilidade de codificação e decodificação por meio de arquivo, onde o arquivo de entrada (já implementado) 'entrada.txt' gera uma resposta no arquivo de saída 'saidaArvoreB.txt' que é o mesmo utilizado na decodificação de um texto por arquivo.

Para o funcionamento desta árvore utiliza-se três estruturas (TInfo, TAB e TDados). Ela também possui as seguintes funções implementadas:

1. TInfo *informacao(int cf1, int cf2, float f);
2. TAB *cria(int t);
3. TAB *libera(TAB *a);
4. void imprime(TAB *a, int andar);
5. void imprime_categoria_1(TAB *a, int tipo);
6. void imprime_categoria_2(TAB *a, int tipo);
7. TAB *busca(TAB* x, char ch);
8. TAB *inicializa();
9. TAB *divisao(TAB *x, int i, TAB* y, int t);
10. TAB *insere_nao_completo(TAB *x, TInfo *in, char k, int t);
11. TAB *insere(TAB *T, TInfo *I, char k, int t);
12. TAB* remover(TAB* arv, char ch, TInfo *in, int t);
13. TAB* retira(TAB* arv, char k, TInfo *in, int t);
14. void codifica_letra(TAB* x, char ch, int andar, char *s);
15. char* decodifica_letra(TAB *x, char *lugar, int j, int t);
16. void busca_exclui_cat_1(TAB *a, int tipo, char *resp);
17. void busca_exclui_cat_2(TAB *a, int tipo, char *resp);
18. TAB *exclui_lista(TAB *a, char *lista, int t);
19. TAB *exclui_categoria(TAB *a, int tipo, int categoria, int t);
20. void codifica_arquivo(TAB *arvore, char *nomeEntrada, char *nomeSaida);
21. char *codifica(TAB *arvore, char *lista);
22. void decodifica_arquivo(TAB *arvore, char* nomeEntrada, char *nomeSaida);
23. char *decodifica(TAB *arvore, char*lista);
24. void mudar_frequencia(TAB* x, char ch, float frequencia);
25. TAB *montar_arvore(int t);
26. void busca_imprime(char c, TAB *a);

---

## Arvore de Huffman

Incompleta.

---

## Menu

O menu apresenta todas as opções necessárias para a utilização da árvore B.