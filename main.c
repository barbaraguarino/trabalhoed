#include "tarvb.h"

// ----------------------- MAIN ------------------

int main(int argc, char const *argv[]){
  int op, aux, t;
  char let;
  TAB *arvore = inicializa();
  inserirT();
  scanf("%d", &t);
  if(t >= 2) arvore = montar_arvore(t);
  else{
    printf("\nValor de T invalido\n");
    exit(1);
  }
  int c1, c2;
  float f;

  printf("\nVamos começar...\n\n");
  while(1){
    imprime_menu_principal();
    scanf("%d", &op);

    switch(op)
    {
      case 1 :
        //Imprime Arvore
        printf("\n");
        imprime(arvore,0);
        printf("\n");
        break;
      
      case 2 :
        //Remover Elemento
        printf("\nLetra: ");
        scanf(" %[^\n]", &let);
        arvore = retira(arvore, let, NULL, t);
        printf("\n");
        imprime(arvore,0);
        printf("\n");
        break;  
      
      case 3 :
        //Buscar Letra
        printf("\nLetra: ");
        scanf(" %[^\n]", &let);
        busca_imprime(let, arvore);
        printf("\n");
        break;
      
      case 4 :
        //Excluir Categoria
        arvore = funcao_opcao_4(arvore, t);
        break;
      
      case 5 :
        //Imprime Categoria
        funcao_opcao_5(arvore);
        break;
      
      case 6 :
        //Mudar frequencia
        printf("\nLetra: ");
        scanf(" %[^\n]", &let);
        float f;
        printf("Frequencia: ");
        scanf("%f", &f);
        mudar_frequencia(arvore,let,f);
        printf("\n");
        break;  
      
      case 7 :
        //Codificar
        funcao_opcao_7(arvore);
        break;
      
      case 8 :
        //Decodificar
        funcao_opcao_8(arvore);
        break;

      case 9 :
        //Inserir Elemento
        printf("\nLetra: ");
        scanf(" %[^\n]", &let);
        printf("Categoria 1 (1/Vogal - 0/Consoante): ");
        scanf("%d", &c1);
        printf("Categoria 2 (1/Maiculo - 0/Minusculo): ");
        scanf("%d", &c2);
        printf("Frequencia: ");
        scanf("%f", &f);
        TInfo *p = informacao(c1, c2, f);
        arvore = insere(arvore, p, let, t);
        printf("\n");
        imprime(arvore,0);
        printf("\n");
        break;

      case 0:
        printf("\nObrigada por utilizar o nosso programa.\n");
        printf("\n");
        imprime(arvore,0);
        libera(arvore);
        exit(1);
        break;
          
      default :
        printf ("Valor invalido!\n");
    }
  }
  return 0;
}