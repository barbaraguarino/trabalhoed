#include "tarvb.h"

void imprime_dados(TDados *p)
{
  printf("%s, %d, %d, %f\n",p->letra, p->c1, p->c2, p->frequencia);
}

TDados *dado(char *letra, int c1, int c2, float f)
{
  TDados *p = (TDados *) malloc(sizeof(TDados));
  if (p) memset(p, 0, sizeof(TDados));
  strcpy(p->letra, letra);
  p->c1 = c1;
  p->c2 = c2;
  p->frequencia = f;
  return p;
}

void salva_dados(TDados *p, FILE *out)
{
  fwrite(p->letra, sizeof(char), sizeof(p->letra), out);
  fwrite(&p->c1, sizeof(int), 1, out);
  fwrite(&p->c2, sizeof(int), 1, out);
  fwrite(&p->frequencia, sizeof(float), 1, out);
}


TDados *le_dados(FILE *in)
{
  TDados *p = (TDados *) malloc(sizeof(TDados));
  if (0 >= fread(p->letra, sizeof(char), sizeof(p->letra), in)) {
    free(p);
    return NULL;
  }
  fread(&p->c1, sizeof(int), 1, in);
  fread(&p->c2, sizeof(int), 1, in);
  fread(&p->frequencia, sizeof(float), 1, in);
  return p;
}

TDados *le_dadosTXT(FILE *in){
  TDados *p = (TDados *) malloc(sizeof(TDados));
  if (0 > fscanf(in, "%s %d %d %f", p->letra, &p->c1, &p->c2, &p->frequencia)) {
    free(p);
    return NULL;
  }else return p;
}

int long tamanho_dados_bytes()
{
  return sizeof(char) * 3 + // letra
    sizeof(int) + // categoria 1
    sizeof(int) + // categoria 2
    sizeof(float); // frequencia
}


TInfo *informacao(int cf1, int cf2, float f){
  TInfo* novo = (TInfo*)malloc(sizeof(TInfo));
  novo->c1 = cf1;
  novo->c2 = cf2;
  novo->fre = f;
  return novo;
}

TAB *cria(int t){
  TAB* novo = (TAB*)malloc(sizeof(TAB));
  novo->nchaves = 0;
  novo->chave =(char*)malloc(sizeof(char*)*((t*2)-1));
  novo->folha=1;
  novo->filho = (TAB**)malloc(sizeof(TAB*)*t*2);
  novo->info = (TInfo**)malloc(sizeof(TInfo*)*((t*2)-1));
  int i;
  for(i=0; i<(t*2); i++) novo->filho[i] = NULL;
  for(i=0; i<(t*2)-1; i++) novo->info[i] = NULL;
  return novo;
}


TAB *libera(TAB *a){
  if(a){
    if(!a->folha){
      int i;
      for(i = 0; i <= a->nchaves; i++) libera(a->filho[i]);
    }
    free(a->chave);
    free(a->filho);
    free(a->info);
    free(a);
    return NULL;
  }
}


void imprime(TAB *a, int andar){
  if(a){
    int i,j;
    for(i=0; i<=a->nchaves-1; i++){
      imprime(a->filho[i],andar+1);
      for(j=0; j<=andar; j++) printf("   ");
      printf("%c\n", a->chave[i]);
    }
    imprime(a->filho[i],andar+1);
  }
}

void imprime_categoria_1(TAB *a, int tipo){
  if(a){
    int i,j;
    for(i=0; i<=a->nchaves-1; i++){
      imprime_categoria_1(a->filho[i], tipo);
      TInfo *in = a->info[i];
      if(in->c1==tipo) printf("%c \t %d \t %d \t %2.3f\n", a->chave[i], in->c1, in->c2, in->fre);
    }
    imprime_categoria_1(a->filho[i], tipo);
  }
}

void imprime_categoria_2(TAB *a, int tipo){
  if(a){
    int i,j;
    for(i=0; i<=a->nchaves-1; i++){
      imprime_categoria_2(a->filho[i],tipo);
      TInfo *in = a->info[i];
      if(in->c2==tipo) printf("%c \t %d \t %d \t %2.3f\n", a->chave[i], in->c1, in->c2, in->fre);
    }
    imprime_categoria_2(a->filho[i], tipo);
  }
}

TAB *busca(TAB* x, char ch){
  TAB *resp = NULL;
  if(!x) return resp;
  int i = 0;
  while(i < x->nchaves && ch > x->chave[i]) i++;
  if(i < x->nchaves && ch == x->chave[i]) return x;
  if(x->folha) return resp;
  return busca(x->filho[i], ch);
}


TAB *inicializa(){
  return NULL;
}


TAB *divisao(TAB *x, int i, TAB* y, int t){
  TAB *z=cria(t);
  z->nchaves= t - 1;
  z->folha = y->folha;
  int j;
  for(j=0;j<t-1;j++){ 
    z->chave[j] = y->chave[j+t];
    //INFORMAÇÃO NÓ
    z->info[j] = y->info[j+t];
  }
  if(!y->folha){
    for(j=0;j<t;j++){
      z->filho[j] = y->filho[j+t];
      y->filho[j+t] = NULL;
    }
  }
  y->nchaves = t-1;
  for(j=x->nchaves; j>=i; j--) x->filho[j+1]=x->filho[j];
  x->filho[i] = z;
  for(j=x->nchaves; j>=i; j--){
    x->chave[j] = x->chave[j-1];
    //INFORMAÇÃO NÓ
    x->info[j] = x->info[j-1];
  }
  //INFORMAÇÃO NÓ
  x->info[i-1] = y->info[t-1];
  x->chave[i-1] = y->chave[t-1];
  x->nchaves++;
  return x;
}


TAB *insere_nao_completo(TAB *x, TInfo *in, char k, int t){
  int i = x->nchaves-1;
  if(x->folha){
    while((i>=0) && (k<x->chave[i])){
      //INFORMAÇÃO NÓ
      x->info[i+1] = x->info[i];
      x->chave[i+1] = x->chave[i];
      i--;
    }
    x->chave[i+1] = k;
    //INFORMAÇÃO NÓ
    x->info[i+1] = in;
    x->nchaves++;
    return x;
  }
  while((i>=0) && (k<x->chave[i])) i--;
  i++;
  if(x->filho[i]->nchaves == ((2*t)-1)){
    x = divisao(x, (i+1), x->filho[i], t);
    if(k>x->chave[i]) i++;
  }
  x->filho[i] = insere_nao_completo(x->filho[i], in, k, t);
  return x;
}


TAB *insere(TAB *T, TInfo *I, char k, int t){
  if(busca(T,k)) return T;
  if(!T){
    T=cria(t);
    T->chave[0] = k;
    //INFORMAÇÃO NÓ
    T->info[0] = I;
    T->nchaves=1;
    return T;
  }
  if(T->nchaves == (2*t)-1){
    TAB *S = cria(t);
    S->nchaves=0;
    S->folha = 0;
    S->filho[0] = T;
    S = divisao(S,1,T,t);
    S = insere_nao_completo(S,I,k,t);
    return S;
  }
  T = insere_nao_completo(T,I,k,t);
  return T;
}


TAB* remover(TAB* arv, char ch, TInfo *in, int t){
  if(!arv) return arv;
  int i;
  for(i = 0; i<arv->nchaves && arv->chave[i] < ch; i++);
  if(i < arv->nchaves && ch == arv->chave[i]){
    if(arv->folha){
      int j;
      for(j=i; j<arv->nchaves-1;j++){
        arv->chave[j] = arv->chave[j+1];
        //INFORMAÇÃO NÓ
        arv->info[j] = arv->info[j+1];
      }
      arv->nchaves--;
      return arv;      
    }
    if(!arv->folha && arv->filho[i]->nchaves >= t){
      TAB *y = arv->filho[i];  
      while(!y->folha) y = y->filho[y->nchaves];
      char temp = y->chave[y->nchaves-1];
      //INFORMAÇÃO NÓ
      TInfo *temp_info = y->info[y->nchaves-1];
      arv->filho[i] = remover(arv->filho[i], temp, temp_info, t); 
      arv->chave[i] = temp;
      //INFORMAÇÃO NÓ
      arv->info[i] = temp_info;
      return arv;
    }
    if(!arv->folha && arv->filho[i+1]->nchaves >= t){
      TAB *y = arv->filho[i+1];  
      while(!y->folha) y = y->filho[0];
      char temp = y->chave[0];
      //INFORMAÇÃO NÓ
      TInfo *temp_info = y->info[0];
      y = remover(arv->filho[i+1], temp, temp_info, t);
      arv->chave[i] = temp;
      //INFORMAÇÃO NÓ
      arv->info[i] = temp_info;
      return arv;
    }
    if(!arv->folha && arv->filho[i+1]->nchaves == t-1 && arv->filho[i]->nchaves == t-1){ //CASO 2C
    
      TAB *y = arv->filho[i];
      TAB *z = arv->filho[i+1];
      y->chave[y->nchaves] = ch;
      //INFORMAÇÃO NÓ
      y->info[y->nchaves] = in;          
      int j;
      for(j=0; j<t-1; j++){               
        y->chave[t+j] = z->chave[j];
        //INFORMAÇÃO NÓ
        y->info[t+j] = z->info[j];
      }
      for(j=0; j<=t; j++)                 
        y->filho[t+j] = z->filho[j];
      y->nchaves = 2*t-1;
      for(j=i; j < arv->nchaves-1; j++){   
        arv->chave[j] = arv->chave[j+1];
        //INFORMAÇÃO NÓ
        arv->info[j] = arv->info[j+1];
      }
      for(j=i+1; j <= arv->nchaves; j++)  
        arv->filho[j] = arv->filho[j+1];
      arv->filho[j] = NULL; 
      arv->nchaves--;
      arv->filho[i] = remover(arv->filho[i], ch, in, t);
      return arv;   
    }   
  }

  TAB *y = arv->filho[i], *z = NULL;
  if (y->nchaves == t-1){ 
    if((i < arv->nchaves) && (arv->filho[i+1]->nchaves >=t)){ 
      z = arv->filho[i+1];
      y->chave[t-1] = arv->chave[i]; 
      //INFORMAÇÃO NÓ
      y->info[t-1] = arv->info[i];  
      y->nchaves++;
      arv->chave[i] = z->chave[0]; 
      //INFORMAÇÃO NÓ
      arv->info[i] = z->info[0];    
      int j;
      for(j=0; j < z->nchaves-1; j++){ 
        z->chave[j] = z->chave[j+1];
        //INFORMAÇÃO NÓ
        z->info[j] = z->info[j+1];
      }
      y->filho[y->nchaves] = z->filho[0]; 
      for(j=0; j < z->nchaves; j++)       
        z->filho[j] = z->filho[j+1];
      z->nchaves--;
      arv->filho[i] = remover(arv->filho[i], ch, in, t);
      return arv;
    }
    if((i > 0) && (!z) && (arv->filho[i-1]->nchaves >=t)){ 
      z = arv->filho[i-1];
      int j;
      for(j = y->nchaves; j>0; j--) {
        y->chave[j] = y->chave[j-1];
        //INFORMAÇÃO NÓ
        y->info[j] = y->info[j-1];
      }
      for(j = y->nchaves+1; j>0; j--)      
        y->filho[j] = y->filho[j-1];
      y->chave[0] = arv->chave[i-1];        
      y->nchaves++;
      arv->chave[i-1] = z->chave[z->nchaves-1]; 
      //INFORMAÇÃO NÓ
      arv->info[i-1] = z->info[z->nchaves-1];
      y->filho[0] = z->filho[z->nchaves];  
      z->nchaves--;
      arv->filho[i] = remover(y, ch, in, t);
      return arv;
    }
    if(!z){ 
      if(i < arv->nchaves && arv->filho[i+1]->nchaves == t-1){
        z = arv->filho[i+1];
        y->chave[t-1] = arv->chave[i];
        //INFORMAÇÃO NÓ
        y->info[t-1] = arv->info[i] ;    
        y->nchaves++;
        int j;
        for(j=0; j < t-1; j++){
          y->chave[t+j] = z->chave[j];  
          //INFORMAÇÃO NÓ
          y->info[t+j] = z->info[j];   
          y->nchaves++;
        }
        if(!y->folha){
          for(j=0; j<t; j++){
            y->filho[t+j] = z->filho[j];
          }
        }
        for(j=i; j < arv->nchaves-1; j++){ 
          arv->chave[j] = arv->chave[j+1];
          //INFORMAÇÃO NÓ
          arv->info[j] = arv->info[j+1];
          arv->filho[j+1] = arv->filho[j+2];
        }
        arv->nchaves--;
        arv = remover(arv, ch, in, t);
        return arv;
      }
      if((i > 0) && (arv->filho[i-1]->nchaves == t-1)){ 
        z = arv->filho[i-1];
        if(i == arv->nchaves){
          z->chave[t-1] = arv->chave[i-1]; 
          //INFORMAÇÃO NÓ
          z->info[t-1] = arv->info[i-1];
        }
        else{
          z->chave[t-1] = arv->chave[i]; 
          //INFORMAÇÃO NÓ
          z->info[t-1] = arv->info[i];  
        }
        z->nchaves++;
        int j;
        for(j=0; j < t-1; j++){
          z->chave[t+j] = y->chave[j];  
          //INFORMAÇÃO NÓ
          z->info[t+j] = y->info[j];  
          z->nchaves++;
        }
        if(!z->folha){
          for(j=0; j<t; j++){
            z->filho[t+j] = y->filho[j];
          }
        }
        arv->nchaves--;
        arv->filho[i-1] = z;
        arv = remover(arv, ch, in, t);
        return arv;
      }
    }
  }  
  arv->filho[i] = remover(arv->filho[i], ch, in, t);
  return arv;
}


TAB* retira(TAB* arv, char k, TInfo *in, int t){
  if(!arv || !busca(arv, k)) return arv;
  return remover(arv, k, in, t);
}

void codifica_letra(TAB* x, char ch, int andar, char *s){
  char *aux = (char*)malloc(sizeof(char)*10);
  int i = 0;
  while(i < x->nchaves && ch > x->chave[i]) i++;
  if(i < x->nchaves && ch == x->chave[i]){
    sprintf(aux, "%i", i);
    strcat(s, aux);
    sprintf(aux, "%i", andar);
    char *aux2 = (char*)malloc(sizeof(char)*10);
    strcpy(aux2, aux);
    strcat(aux2, s);
    strcpy(s, aux2);
    free(aux2);
    free(aux);
    return;
  }
  if(x->folha){
    strcpy(aux, "?");
    strcpy(s, aux);
    free(aux);
    return;
  }
  sprintf(aux, "%i", i);
  strcat(s, aux);
  codifica_letra(x->filho[i], ch, (andar+1), s);
  free(aux);
}

char* decodifica_letra(TAB *x, char *lugar, int j, int t){
  if(j<t){
    if(j==(t-1)){
      char aux = lugar[j];
      int i = aux - '0';
      char aux2[2];
      char *resp = (char*)malloc(sizeof(char)*3);
      aux2[0] = x->chave[i];
      strcpy(resp, aux2);
      return resp;
    }else if(x->folha){
      char *resp = (char*)malloc(sizeof(char)*3);
      strcpy(resp, "?");
      return resp;
    }else{
      char aux = lugar[j];
      int i = aux - '0';
      if(x->filho[i]) return decodifica_letra(x->filho[i], lugar, (j+1), t);
      else{
        char *resp = (char*)malloc(sizeof(char)*3);
        strcpy(resp, "?");
        return resp;
      }
    }
  }
}


void busca_exclui_cat_1(TAB *a, int tipo, char *resp){
  char aux[2];
  if(a){
    int i;
    for(i=0; i<=a->nchaves-1; i++){
      busca_exclui_cat_1(a->filho[i], tipo, resp);
      TInfo *in = a->info[i];
      if(in->c1 == tipo){
        aux[0] = a->chave[i];
        strcat(resp, aux);
      }
    }
    busca_exclui_cat_1(a->filho[i],tipo,resp);
  }
}

void busca_exclui_cat_2(TAB *a, int tipo, char *resp){
  char aux[2];
  if(a){
    int i;
    for(i=0; i<=a->nchaves-1; i++){
      busca_exclui_cat_2(a->filho[i], tipo, resp);
      TInfo *in = a->info[i];
      if(in->c2 == tipo){
        aux[0] = a->chave[i];
        strcat(resp, aux);
      }
    }
    busca_exclui_cat_2(a->filho[i],tipo,resp);
  }
}

TAB *exclui_lista(TAB *a, char *lista, int t){

  int i = strlen(lista), j;
  for(j=0;j<i;j++){
    a = retira(a, lista[j], NULL, t);
  }
  return a;

}

TAB *exclui_categoria(TAB *a, int tipo, int categoria, int t){
  if(categoria == 1 && (a)){
    char *listaResp1 = (char *)malloc(sizeof(char)*30);
    busca_exclui_cat_1(a, tipo, listaResp1);
    a = exclui_lista(a, listaResp1, t);
    free(listaResp1);
    return a;
  }else if(categoria == 2 && (a)){
    char *listaResp2 = (char *)malloc(sizeof(char)*30);
    busca_exclui_cat_2(a, tipo, listaResp2);
    a = exclui_lista(a, listaResp2, t);
    free(listaResp2);
    return a;
  }else return a;
}

void codifica_arquivo(TAB *arvore, char *nomeEntrada, char *nomeSaida){
  FILE *entrada, *saida;
  entrada = fopen(nomeEntrada, "r");
  saida = fopen(nomeSaida, "w+");
  if(!entrada || !saida) exit(1);
  char letra[2];
  char *aux = (char*)malloc(sizeof(char)*20);
  while(fscanf(entrada, "%c",letra) > 0){
    codifica_letra(arvore, letra[0], 0, aux);
    fprintf(saida, "%s", aux);
    strcpy(aux, "");
  }
  free(aux);
  fclose(entrada);
  fclose(saida);
}

char *codifica(TAB *arvore, char *lista){
  char *resp = (char*)malloc(sizeof(char)*500);
  strcpy(resp, "");
  char *aux = (char*)malloc(sizeof(char)*20);
  strcpy(aux, "");
  int tam = strlen(lista), i;
  for(i=0; i<tam; i++){
    codifica_letra(arvore, lista[i], 0, aux);
    strcat(resp, aux);
    strcpy(aux, "");
  }
  free(aux);
  return resp;
}
//Vamos Tentar Com Espaçoes e NOmes Barbara
void decodifica_arquivo(TAB *arvore, char* nomeEntrada, char *nomeSaida){
  FILE *entrada, *saida;
  entrada = fopen(nomeEntrada, "r");
  saida = fopen(nomeSaida, "w");
  if(!entrada || !saida) exit(1);
  int i = 0, j = 0, x;
  char *u = (char*)malloc(sizeof(char)*20);
  strcpy(u, "");
  char lista[2];
  while(fscanf(entrada, "%c", lista) > 0){
    char a = lista[0];
    if(a != '?'){
      int k = a - '0';
      for(x=0; x<k+1; x++){
        if(fscanf(entrada, "%c", lista)<0) break;
        u[x] = lista[0];
      }
      char *c = decodifica_letra(arvore, u, 0, (k+1));
      fprintf(saida, "%c", c[0]);
    }else{
      fprintf(saida, "%s", "?");
    }
  }
  free(u);
  fclose(saida);
  fclose(entrada);
  
}


char *decodifica(TAB *arvore, char*lista){
  int tam = strlen(lista), i = 0, j = 0, x;
  char *resp = (char *)malloc(sizeof(char)*500);
  strcpy(resp, "");
  char *u = (char*)malloc(sizeof(char)*20);
  while(i<tam){
    char a = lista[i];
    if(a != '?'){
      int k = a - '0';
      for(x=0; x<k+1; x++){
        i = i+1;
        u[x] = lista[i];
      }
      char *c = decodifica_letra(arvore, u, 0, (k+1));
      strcat(resp, c);
    }else{
      strcat(resp,"?");
    }
    i = i+1;
  }
  free(u);
  return resp;
}

void mudar_frequencia(TAB* x, char ch, float frequencia){
  if(!x) return;
  int i = 0;
  while(i < x->nchaves && ch > x->chave[i]) i++;
  if(i < x->nchaves && ch == x->chave[i]){
    TInfo *sInfo = x->info[i];
    sInfo->fre = frequencia;
    return;
  }
  if(x->folha) return;
  mudar_frequencia(x->filho[i], ch, frequencia);
}

TAB *montar_arvore(int t){
  TAB * arvore = inicializa();
  FILE *arquivo = fopen("dados.b", "rb+");
  if(!arquivo) return NULL;
  int verifica = 0;
  while(1){
    TDados *dados = le_dados(arquivo);
    if(!dados && verifica == 1) break;
    if(!dados && verifica == 0){
      fseek(arquivo, tamanho_dados_bytes(), SEEK_SET);
      verifica = 1;
      dados = le_dados(arquivo);
    }
    TInfo *p = informacao(dados->c1, dados->c2, dados->frequencia);
    char l = dados->letra[0];
    arvore = insere(arvore, p, l, t);
    fseek(arquivo, tamanho_dados_bytes(), SEEK_CUR);
  }
  fclose(arquivo);
  return arvore;
}

void busca_imprime(char c, TAB *a){
  if(a){
    TAB *resp = busca(a, c);
    int i;
    for(i=0; i<resp->nchaves; i++){
      if(resp->chave[i]==c){
        TInfo *in = resp->info[i];
        printf("\n%c, %d, %d, %.3f\n",resp->chave[i], in->c1, in->c2, in->fre);
        return;
      }
    }
  }
}

void inserirT(){
  printf("\n---- Bem vindo ao programa da Arvore B ----\n");
  printf("\nPara começão o a utilizar o programa, \npor favor insera o T que sera utilizado na arvore: ");
}

void imprime_menu_principal(){
  printf("1 - Imprime Arvore\n");
  printf("2 - Remover Elemento\n");
  printf("3 - Buscar Letra\n");
  printf("4 - Excluir Classificação\n");
  printf("5 - Imprime Classificação\n");
  printf("6 - Mudar Frequencia\n");
  printf("7 - Codificar\n");
  printf("8 - Decodificar\n");
  printf("9 - Inserir Elemento\n");
  printf("0 - Sair\n");

  printf("\nOpção: ");
}

void imprime_menu_opcao_4(){
  printf("\n1 - Classificação 1 (Vogal ou Cosoante)\n");
  printf("2 - Classificação 2 (Maiscula ou Minuscula)\n");
  printf("3 - Voltar\n");
  printf("4 - Fechar Programa\n");
  printf("\nOpção: ");
}

void imprime_menu_opcao_4_1(){
  printf("\n1 - Excluir Vogal\n");
  printf("2 - Excluir Consoante\n");
  printf("3 - Voltar\n");
  printf("4 - Fechar Programa\n");
  printf("\nOpção: ");
}

void imprime_menu_opcao_4_2(){
  printf("\n1 - Excluir Maisculo\n");
  printf("2 - Excluir Minusculo\n");
  printf("3 - Voltar\n");
  printf("4 - Fechar Programa\n");
  printf("\nOpção: ");
}

void imprime_menu_opcao_5(){
  printf("\n1 - Classificação 1 (Vogal ou Cosoante)\n");
  printf("2 - Classificação 2 (Maiscula ou Minuscula)\n");
  printf("3 - Voltar\n");
  printf("4 - Fechar Programa\n");
  printf("\nOpção: ");
}

void imprime_menu_opcao_5_1(){
  printf("\n1 - Imprimir Vogal\n");
  printf("2 - Imprimir Consoante\n");
  printf("3 - Voltar\n");
  printf("4 - Fechar Programa\n");
  printf("\nOpção: ");
}

void imprime_menu_opcao_5_2(){
  printf("\n1 - Imprimir Maisculo\n");
  printf("2 - Imprimir Minusculo\n");
  printf("3 - Voltar\n");
  printf("4 - Fechar Programa\n");
  printf("\nOpção: ");
}

void subMenu(){
  printf("\nDeseja mostar o menu novamente (1 para sim e 0 para não): ");
}

void imprime_menu_opcao_7_and_8(){
  printf("\n1 - Utilizando Arquivo\n");
  printf("2 - Utilizando Terminal\n");
  printf("3 - Voltar\n");
  printf("4 - Fechar Programa\n");
  printf("\nOpção: ");
}

void funcao_opcao_5(TAB *arvore){
  int op;
  while(1){
    imprime_menu_opcao_5();
    scanf("%d", &op);

    switch(op)
    {
      case 1 :
        //Imprimir Categoria 1
        funcao_opcao_5_and_1(arvore);
        break;
      
      case 2 :
        //Imprimir Categoria 2
        funcao_opcao_5_and_2(arvore);
        break;  
      
      case 3 :
        return;
        break;
      
      case 4 :
        printf("\nObrigada por utilizar o nosso programa.\n");
        printf("\n");
        imprime(arvore,0);
        libera(arvore);
        exit(1);
        break;
          
      default :
        printf ("Valor invalido!\n");
    }
  }
}

void funcao_opcao_5_and_2(TAB *arvore){
  int op;
  while(1){
    imprime_menu_opcao_5_2();
    scanf("%d", &op);

    switch(op)
    {
      case 1 :
        //Imprime Categoria 2 
        //Maiscula
        printf("\n");
        imprime_categoria_2(arvore, 1);
        printf("\n");
        break;
      
      case 2 :
        //Imprime Categoria 2 
        //Minuscula
        printf("\n");
        imprime_categoria_2(arvore, 0);
        printf("\n");
        break; 
      
      case 3 :
        //Voltar
        return ;
        break;
      
      case 4 :
        printf("\nObrigada por utilizar o nosso programa.\n");
        printf("\n");
        imprime(arvore,0);
        libera(arvore);
        break;
          
      default :
        printf ("Valor invalido!\n");
    }
  }
}

void funcao_opcao_5_and_1(TAB *arvore){
  int op;
  while(1){
    imprime_menu_opcao_5_1();
    scanf("%d", &op);

    switch(op)
    {
      case 1 :
        //Imprime Categoria 1 
        //Maiscula
        printf("\n");
        imprime_categoria_1(arvore, 1);
        printf("\n");
        break;
      
      case 2 :
        //Imprime Categoria 1
        //Minuscula
        printf("\n");
        imprime_categoria_1(arvore, 0);
        printf("\n");
        break; 
      
      case 3 :
        //Voltar
        return ;
        break;
      
      case 4 :
        printf("\nObrigada por utilizar o nosso programa.\n");
        printf("\n");
        imprime(arvore,0);
        libera(arvore);
        exit(1);
        break;
          
      default :
        printf ("Valor invalido!\n");
    }
  }
}

void funcao_opcao_8(TAB *arvore){
  int op, aux;
  while(1){
    imprime_menu_opcao_7_and_8();
    scanf("%d", &op);

    switch(op)
    {
      case 1 :
        //Decodificar Utilizando Arquivo
        printf("\nPossui elemento no arquivo 'saidaArvoreB.txt'(1/SIM - 0/NÂO): ");
        scanf("%d", &aux);
        if(aux == 1)decodifica_arquivo(arvore, "saidaArvoreB.txt", "SDecodificaArvoreB.txt");
        break;
      
      case 2 :{
        //Codificar utilizando o terminal;
        char *lista = (char*)malloc(sizeof(char)*400);
        printf("\nInsira o texto (max 400): ");
        scanf(" %[^\n]", lista);
        printf("\n");
        char *resp = decodifica(arvore,lista);
        printf("Resposta: %s\n",resp);
        free(lista);
        free(resp);
        break;  
      }
      case 3 :
        return;
        break;
      
      case 4 :
        printf("\nObrigada por utilizar o nosso programa.\n");
        printf("\n");
        imprime(arvore,0);
        libera(arvore);
        exit(1);
        break;
          
      default :
        printf ("Valor invalido!\n");
    }
  }
}

void funcao_opcao_7(TAB *arvore){
  int op, aux;
  while(1){
    imprime_menu_opcao_7_and_8();
    scanf("%d", &op);

    switch(op)
    {
      case 1 :
        //Codificar Utilizando Arquivo
        printf("\nPossui elemento no arquivo 'entrada.txt'(1/SIM - 0/NÂO): ");
        scanf("%d", &aux);
        if(aux == 1)codifica_arquivo(arvore, "entrada.txt", "saidaArvoreB.txt");
        break;
      
      case 2 :{
        //Codificar utilizando o terminal;
        char *string = (char*)malloc(sizeof(char)*300);
        printf("\nInsira o texto (max 300): ");
        scanf(" %[^\n]", string);
        char *resp = codifica(arvore, string);
        printf("\n");
        printf("Resposta: %s\n",resp);
        free(string);
        free(resp);
        break;  
      }
      case 3 :
        printf("\n");
        return;
        break;
      
      case 4 :
        printf("\nObrigada por utilizar o nosso programa.\n");
        printf("\n");
        imprime(arvore,0);
        libera(arvore);
        exit(1);
        break;
          
      default :
        printf ("Valor invalido!\n");
    }
  }
}

TAB *funcao_opcao_4(TAB *arvore, int t){
  int op;
  while(1){
    imprime_menu_opcao_4();
    scanf("%d", &op);

    switch(op)
    {
      case 1 :
        //Esxcluir Categoria 2
        arvore = funcao_opcao_4_1(arvore, t);
        break;
      
      case 2 :
        //Esxcluir Categoria 2 
        arvore = funcao_opcao_4_2(arvore, t);
        break;  
      
      case 3 :
        return arvore;
        break;
      
      case 4 :
        printf("\nObrigada por utilizar o nosso programa.\n");
        printf("\n");
        imprime(arvore,0);
        libera(arvore);
        exit(1);
        break;
          
      default :
        printf ("Valor invalido!\n");
    }
  }
  return arvore;
}

TAB *funcao_opcao_4_2(TAB *arvore, int t){
  int op;
  while(1){
    imprime_menu_opcao_4_2();
    scanf("%d", &op);

    switch(op)
    {
      case 1 :
        //Esxcluir Categoria 2 
        //Maiscula
        arvore = exclui_categoria(arvore, 1, 2, t);
        printf("\n");
        imprime(arvore, 0);
        printf("\n");
        break;
      
      case 2 :
        //Esxcluir Categoria 2 
        //Minuscula
        arvore = exclui_categoria(arvore, 0, 2, t);
        printf("\n");
        imprime(arvore, 0);
        printf("\n");
        break;  
      
      case 3 :
        return arvore;
        break;
      
      case 4 :
        printf("\nObrigada por utilizar o nosso programa.\n");
        printf("\n");
        imprime(arvore,0);
        libera(arvore);
        exit(1);
        break;
          
      default :
        printf ("Valor invalido!\n");
    }
  }
  return arvore;
}

TAB *funcao_opcao_4_1(TAB *arvore, int t){
  int op;
  while(1){
    imprime_menu_opcao_4_1();
    scanf("%d", &op);

    switch(op)
    {
      case 1 :
        //Esxcluir Categoria 1 
        //Vogal
        arvore = exclui_categoria(arvore, 1, 1, t);
        printf("\n");
        imprime(arvore, 0);
        printf("\n");
        break;
      
      case 2 :
        //Esxcluir Categoria 1 
        //Consoante
        arvore = exclui_categoria(arvore, 0, 1, t);
        printf("\n");
        imprime(arvore, 0);
        printf("\n");
        break;  
      
      case 3 :
        return arvore;
        break;
      
      case 4 :
        printf("\nObrigada por utilizar o nosso programa.\n");
        printf("\n");
        imprime(arvore,0);
        libera(arvore);
        exit(1);
        break;
          
      default :
        printf ("Valor invalido!\n");
    }
  }
  return arvore;
}
