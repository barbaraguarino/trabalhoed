#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct inf{
	int c1, c2; // c1 = vogal(1) ou consoante(0) / c2 = maiscula(1) ou minuscula(0)
	float fre;
}TInfo;

typedef struct ArvB{
  int nchaves, folha;
  char *chave;
  struct ArvB **filho;
  struct TInfo **info;
}TAB;

typedef struct pizza {
	char letra[3];
	int c1;
	int c2;
	float frequencia;
}TDados;

//Cria a estrutura de informações
TInfo *informacao(int cf1, int cf2, float f);
//Cria um nó da arvore
TAB *cria(int t);
//Livera a arvore
TAB *libera(TAB *a);
//Imprime a arvore
void imprime(TAB *a, int andar);
//Imprime categoria 1 de acordo com a informação
void imprime_categoria_1(TAB *a, int tipo);
//Imprime categoria 2 de acrodo com a informação
void imprime_categoria_2(TAB *a, int tipo);
//Busca informação de acordo com a chave
TAB *busca(TAB* x, char ch);
//Inicializa a arvore
TAB *inicializa();
//Funções de para inserção na arvore
TAB *divisao(TAB *x, int i, TAB* y, int t);
TAB *insere_nao_completo(TAB *x, TInfo *in, char k, int t);
TAB *insere(TAB *T, TInfo *I, char k, int t);
//Funções para remover da arvore
TAB* remover(TAB* arv, char ch, TInfo *in, int t);
TAB* retira(TAB* arv, char k, TInfo *in, int t);
//Função de codificação de uma letra
void codifica_letra(TAB* x, char ch, int andar, char *s);
//Função de decodificação de uma letra
char* decodifica_letra(TAB *x, char *lugar, int j, int t);
//Função que retorna uma lista com letras de acordo com a categoria 1
void busca_exclui_cat_1(TAB *a, int tipo, char *resp);
//Função que retorna uma lista com letras de acordo com a categoria 2
void busca_exclui_cat_2(TAB *a, int tipo, char *resp);
//Esclui a elementos da arvore de acordo com a lista
TAB *exclui_lista(TAB *a, char *lista, int t);
//Função qeu chama o resto;
TAB *exclui_categoria(TAB *a, int tipo, int categoria, int t);
//Codifica de acordo com o arquivo
void codifica_arquivo(TAB *arvore, char *nomeEntrada, char *nomeSaida);
//Função de codificação de acordo com a lista de entrada pelo terminal
char *codifica(TAB *arvore, char *lista);
//Decodifica de acordo com o arquivo
void decodifica_arquivo(TAB *arvore, char* nomeEntrada, char *nomeSaida);
//Decodifica de codificação de acordo com a lista de entrada pelo terminal
char *decodifica(TAB *arvore, char*lista);
//Mudar a frequencia
void mudar_frequencia(TAB* x, char ch, float frequencia);
//Função para mudar a arvore
TAB *montar_arvore(int t);
//Busca um letra e imprime
void busca_imprime(char c, TAB *a);

//Funções utilicadas para criar e ler o arquivo
void imprime_dados(TDados *p);
TDados *dado(char *letra, int c1, int c2, float f);
void salva_dados(TDados *p, FILE *out);
TDados *le_dados(FILE *in);
TDados *le_dadosTXT(FILE *in);
int long tamanho_dados_bytes();

//Funções para o funcionamente do MENU
void inserirT();
void imprime_menu_principal();
void imprime_menu_opcao_4();
void imprime_menu_opcao_4_1();
void imprime_menu_opcao_4_2();
void imprime_menu_opcao_5();
void imprime_menu_opcao_5_1();
void imprime_menu_opcao_5_2();
void subMenu();
void imprime_menu_opcao_7_and_8();
void funcao_opcao_5(TAB *arvore);
void funcao_opcao_5_and_2(TAB *arvore);
void funcao_opcao_5_and_1(TAB *arvore);
void funcao_opcao_8(TAB *arvore);
void funcao_opcao_7(TAB *arvore);
TAB *funcao_opcao_4(TAB *arvore, int t);
TAB *funcao_opcao_4_2(TAB *arvore, int t);
TAB *funcao_opcao_4_1(TAB *arvore, int t);

